package org.boza.pocs.guicedynamicconfig.ui;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.boza.pocs.bootstrap.BoostrapModule;

import javax.swing.*;

/**
 * Hello world!
 *
 */
public class App 
{

    private static Injector injector;

    public static void main( String[] args )
    {
        injector = Guice.createInjector(new BoostrapModule());
        JDialog mainDialog = injector.getInstance(JDialog.class);
        mainDialog.setVisible(true);
    }

    public static Injector getInjector(){
        return injector;
    }
}
