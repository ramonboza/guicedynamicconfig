package org.boza.pocs.guicedynamicconfig.ui;

import com.google.inject.Inject;
import org.boza.pocs.dao.IPasswordCheckerDAO;
import org.boza.pocs.domain.IPasswordCheckService;
import org.boza.pocs.model.enums.CheckType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: ramon.boza
 * Date: 14/10/13
 * Time: 13:52
 * To change this template use File | Settings | File Templates.
 */
public class PasswordCheckDialog extends JDialog {

    @Inject
    private IPasswordCheckService _checkService;

    @Inject
    private IPasswordCheckerDAO _dao;

    private JPasswordField _password;
    private JButton _check;
    private JRadioButton _lengthButton;
    private JRadioButton _alphaButton;
    private JRadioButton _alphaLengthButton;
    private JRadioButton _noCheck;
    private JLabel _messageLabel;

    public PasswordCheckDialog(){
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        init();
    }

    private void init() {
        setSize(400,200);
        JPanel mainPanel = new JPanel();
        add(mainPanel);
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
        _password = new JPasswordField();
        _password.setAlignmentX(LEFT_ALIGNMENT);
        mainPanel.add(_password);

        _check = new JButton("Check");
        _check.setAlignmentX(LEFT_ALIGNMENT);
        _check.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkPassword();
            }
        });
        mainPanel.add(_check);


        JPanel buttonsPanel = createButtons();
        updateButtonsEvents();

        mainPanel.add(buttonsPanel);
        buttonsPanel.setAlignmentX(LEFT_ALIGNMENT);

        _messageLabel = new JLabel("message");
        _messageLabel.setForeground(Color.red);
        _messageLabel.setAlignmentX(LEFT_ALIGNMENT);

        mainPanel.add(_messageLabel);

    }



    private void checkPassword() {
        String psw = new String(_password.getPassword());
        boolean checkIt = _checkService.check(psw);
        String message = "";
        if(checkIt){
            message = "Perfect!";
        }else{
            message = "there were problems";
        }
        _messageLabel.setText(message);

    }

    private JPanel createButtons() {
        ButtonGroup group = new ButtonGroup();

        _lengthButton = new JRadioButton("Length");
        _alphaButton = new JRadioButton("Alpha");
        _alphaLengthButton = new JRadioButton("Alpha-Length");
        _noCheck = new JRadioButton("No Check");

        group.add(_lengthButton);
        group.add(_alphaButton);
        group.add(_alphaLengthButton);
        group.add(_noCheck);

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel,BoxLayout.LINE_AXIS));

        buttonsPanel.add(_lengthButton);
        buttonsPanel.add(_alphaButton);
        buttonsPanel.add(_alphaLengthButton);
        buttonsPanel.add(_noCheck);
        return buttonsPanel;
    }

    private void updateButtonsEvents() {
        _lengthButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Changed to LENGTH");
                _dao.set(CheckType.LENGTH);
            }
        });
        _alphaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Changed to ALPHALENGTH");
                _dao.set(CheckType.ALPHANUMERIC);
            }
        });
        _alphaLengthButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Changed to ALPHA");
                _dao.set(CheckType.ALPHALENGTH);
            }
        });
        _noCheck.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Changed to NONE");
                _dao.set(CheckType.NONE);
            }
        });
    }
}
