package org.boza.pocs.persistence;

import org.boza.pocs.model.enums.CheckType;

/**
 * Created with IntelliJ IDEA.
 * User: ramon.boza
 * Date: 15/10/13
 * Time: 11:58
 * To change this template use File | Settings | File Templates.
 */
public interface PasswordCheckerPersistence {

    CheckType get();

    void set(CheckType type);
}
