package org.boza.pocs.persistence;

import org.boza.pocs.model.enums.CheckType;

/**
 * Created with IntelliJ IDEA.
 * User: ramon.boza
 * Date: 15/10/13
 * Time: 11:59
 * To change this template use File | Settings | File Templates.
 */


public class InMemoryPasswordChecker implements PasswordCheckerPersistence {

    private CheckType type = CheckType.ALPHALENGTH;

    @Override
    public CheckType get() {
        return type;
    }

    @Override
    public void set(CheckType type) {
        this.type = type;
    }
}
