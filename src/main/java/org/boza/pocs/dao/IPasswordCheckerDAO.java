package org.boza.pocs.dao;

import org.boza.pocs.model.enums.CheckType;

/**
 * Created with IntelliJ IDEA.
 * User: ramon.boza
 * Date: 15/10/13
 * Time: 11:53
 * To change this template use File | Settings | File Templates.
 */
public interface IPasswordCheckerDAO {
    CheckType get();

    void set(CheckType type);
}
