package org.boza.pocs.dao;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.boza.pocs.model.enums.CheckType;
import org.boza.pocs.persistence.PasswordCheckerPersistence;

/**
 * Created with IntelliJ IDEA.
 * User: ramon.boza
 * Date: 15/10/13
 * Time: 11:51
 * To change this template use File | Settings | File Templates.
 */

@Singleton
public class PasswordCheckerDAO implements IPasswordCheckerDAO {

    //An in-memory mocked database
    @Inject
    private PasswordCheckerPersistence _persistence;

    @Override
    public CheckType get(){
        return _persistence.get();
    }

    @Override
    public void set(CheckType type){
        _persistence.set(type);
    }
}
