package org.boza.pocs.factories;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import org.boza.pocs.dao.IPasswordCheckerDAO;
import org.boza.pocs.model.*;
import org.boza.pocs.model.enums.CheckType;

/**
 * Created with IntelliJ IDEA.
 * User: ramon.boza
 * Date: 14/10/13
 * Time: 13:40
 * To change this template use File | Settings | File Templates.
 */
@Singleton
public class PasswordRuleCheckerProvider implements Provider<RuleChecker> {

    @Inject
    private IPasswordCheckerDAO dao;

    public RuleChecker get() {
        // it would even be better if you could use singletons here.

        // Retry type from database
        CheckType type = dao.get();

        switch(type) {
            case LENGTH:
                return new LengthCheck();
            case ALPHANUMERIC:
                return new AlphanumericCheck();
            case ALPHALENGTH:
                return new AlphaAndLenghtCheckAdapter();
            case NONE:
            default:
                return new NoCheck();
        }
    }


}