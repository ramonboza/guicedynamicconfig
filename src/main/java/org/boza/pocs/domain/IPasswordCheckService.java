package org.boza.pocs.domain;

/**
 * Created with IntelliJ IDEA.
 * User: ramon.boza
 * Date: 14/10/13
 * Time: 14:09
 * To change this template use File | Settings | File Templates.
 */
public interface IPasswordCheckService {

    /**
     * Checks if the password is correct
     * @param password
     * @return
     */
    boolean check(final String password);
}
