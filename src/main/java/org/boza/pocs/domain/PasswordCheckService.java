package org.boza.pocs.domain;

import com.google.inject.Key;
import com.google.inject.name.Names;
import org.boza.pocs.guicedynamicconfig.ui.App;
import org.boza.pocs.model.RuleChecker;

/**
 * Created with IntelliJ IDEA.
 * User: ramon.boza
 * Date: 14/10/13
 * Time: 14:08
 * To change this template use File | Settings | File Templates.
 */
public class PasswordCheckService implements IPasswordCheckService {
    /*@Inject
    @Named("password")
    private RuleChecker _checker;*/

   /* @Inject
    private PasswordRuleCheckerProvider _provider;*/

    public boolean check(final String password){
        RuleChecker _checker = App.getInjector().getInstance((Key.get(RuleChecker.class, Names.named("password"))));
        return _checker.check(password) > 8;
       /* return _provider.get().check(password) > 8;*/
    }
}
