package org.boza.pocs.bootstrap;

import com.google.inject.AbstractModule;
import org.boza.pocs.dao.IPasswordCheckerDAO;
import org.boza.pocs.dao.PasswordCheckerDAO;
import org.boza.pocs.domain.IPasswordCheckService;
import org.boza.pocs.domain.PasswordCheckService;
import org.boza.pocs.factories.PasswordRuleCheckerProvider;
import org.boza.pocs.guicedynamicconfig.ui.PasswordCheckDialog;
import org.boza.pocs.model.RuleChecker;
import org.boza.pocs.persistence.InMemoryPasswordChecker;
import org.boza.pocs.persistence.PasswordCheckerPersistence;

import javax.swing.*;

import static com.google.inject.name.Names.named;

/**
 * Created with IntelliJ IDEA.
 * User: ramon.boza
 * Date: 14/10/13
 * Time: 13:36
 * To change this template use File | Settings | File Templates.
 */
public class BoostrapModule  extends AbstractModule {

    @Override
    protected void configure() {
        bind(JDialog.class).to(PasswordCheckDialog.class);
        bind(RuleChecker.class).annotatedWith(named("password")).toProvider(PasswordRuleCheckerProvider.class);
        bind(IPasswordCheckService.class).to(PasswordCheckService.class);
        bind(IPasswordCheckerDAO.class).to(PasswordCheckerDAO.class);
        bind(PasswordCheckerPersistence.class).to(InMemoryPasswordChecker.class);
    }
}
