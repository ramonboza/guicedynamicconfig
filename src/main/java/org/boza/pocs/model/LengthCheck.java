package org.boza.pocs.model;

/**
 * Created with IntelliJ IDEA.
 * User: ramon.boza
 * Date: 14/10/13
 * Time: 13:41
 * To change this template use File | Settings | File Templates.
 */
public class LengthCheck implements RuleChecker {
    @Override
    public int check(String pass) {
        int length = pass.length();
        if(length < 7)
            return 0;
        if(length < 15)
            return 5;
        return 10;
    }
}
