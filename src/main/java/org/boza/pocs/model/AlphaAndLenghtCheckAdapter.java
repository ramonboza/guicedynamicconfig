package org.boza.pocs.model;

/**
 * Created with IntelliJ IDEA.
 * User: ramon.boza
 * Date: 14/10/13
 * Time: 13:47
 * To change this template use File | Settings | File Templates.
 */
public class AlphaAndLenghtCheckAdapter implements RuleChecker {

    private AlphanumericCheck alphaCheck;
    private LengthCheck lengthCheck;

    public AlphaAndLenghtCheckAdapter(){
        alphaCheck = new AlphanumericCheck();
        lengthCheck = new LengthCheck();
    }

    @Override
    public int check(String pass) {
        return  (alphaCheck.check(pass)+lengthCheck.check(pass))/2;
    }
}
