package org.boza.pocs.model.enums;

/**
 * Created with IntelliJ IDEA.
 * User: ramon.boza
 * Date: 14/10/13
 * Time: 13:40
 * To change this template use File | Settings | File Templates.
 */
public enum CheckType {
    LENGTH, ALPHANUMERIC, ALPHALENGTH, NONE
}
