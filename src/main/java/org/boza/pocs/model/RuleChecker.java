package org.boza.pocs.model;

/**
 * Created with IntelliJ IDEA.
 * User: ramon.boza
 * Date: 14/10/13
 * Time: 13:38
 * To change this template use File | Settings | File Templates.
 */
public interface RuleChecker {

    /**
     * Checks the {@param pass} and scores it with a value
     */
    int check(final String pass);
}
