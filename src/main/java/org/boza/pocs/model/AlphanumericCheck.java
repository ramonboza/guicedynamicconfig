package org.boza.pocs.model;

/**
 * Created with IntelliJ IDEA.
 * User: ramon.boza
 * Date: 14/10/13
 * Time: 13:42
 * To change this template use File | Settings | File Templates.
 */
public class AlphanumericCheck implements RuleChecker {
    @Override
    public int check(String pass) {
        String pattern= "^[a-zA-Z0-9]+$";
        if(pass.matches(pattern)){
            return 10;
        }
        return 0;
    }
}
